package com.example.diogenes.museudoestado;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.prefs.Preferences;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void agendarVisita(View view) {
        Intent intent = new Intent(this, AgendamentoActivity.class);
        startActivity(intent);
    }

    public void verLocalização(View view) {
        Uri uriGeografico = Uri.parse("geo:0,0?q=Avenida Rui Barbosa, 960 - Graças");
        Intent it = new Intent(Intent.ACTION_VIEW, uriGeografico);
        startActivity(it);
    }

    public void efetuarDoacao(View view) {
        Toast.makeText(this, "Parabéns você doou R$ 10,00 para o Museu do Estado de Pernambuco", Toast.LENGTH_LONG).show();
        Button button = findViewById(R.id.btn_doacao);
        button.setEnabled(false);
    }

    public void verSite(View view) {
        Uri endereco = Uri.parse("http://www.museudoestadope.com.br");
        Intent it = new Intent(Intent.ACTION_VIEW, endereco);
        startActivity(it);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PreferencesUtil.clear(getBaseContext());
    }
}
