package com.example.diogenes.museudoestado;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import java.util.prefs.Preferences;

public class AgendamentoActivity extends AppCompatActivity {

    private EditText editTextNome;
    private CheckBox checkBoxPrimeiraVisita;
    private Spinner spinnerEstados;
    private RadioButton radioButtonMasculino;
    private RadioButton radioButtonFeminino;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agendamento);

        editTextNome = findViewById(R.id.etNome);
        checkBoxPrimeiraVisita = findViewById(R.id.cbPrimeiraVisita);
        spinnerEstados = findViewById(R.id.cbEstados);
        radioButtonMasculino = findViewById(R.id.rbMasculino);
        radioButtonFeminino = findViewById(R.id.rbFeminino);

        if (PreferencesUtil.getAllKeys(getBaseContext()).size() > 0) {
            Log.v("store", "EXISTE PREFERÊNCIAS");
            carregandoDados();
        }
    }

    private void carregandoDados() {
        if (PreferencesUtil.isKeyExists(getBaseContext(), "nome")) {
            editTextNome.setText(PreferencesUtil.getString(getBaseContext(), "nome"));
        }
        if (PreferencesUtil.isKeyExists(getBaseContext(), "primeiraVez")) {
            checkBoxPrimeiraVisita.setChecked(PreferencesUtil.getBoolean(getBaseContext(), "primeiraVez"));
        }
        if (PreferencesUtil.isKeyExists(getBaseContext(), "sexo")) {
            if(PreferencesUtil.getString(getBaseContext(), "sexo").equals("masculino")){
                radioButtonMasculino.setChecked(true);
            } else {
                radioButtonFeminino.setChecked(true);
            }
        }
        if (PreferencesUtil.isKeyExists(getBaseContext(), "estado")) {
//            spinnerEstados.setId(PreferencesUtil.getInterger(getBaseContext(), "estado"));
        }
    }

    public void agendarVisita(View view) {
        PreferencesUtil.setString(getBaseContext(), "nome", editTextNome.getText().toString());
        PreferencesUtil.setBoolean(getBaseContext(), "primeiraVez", checkBoxPrimeiraVisita.isChecked());
        if (radioButtonMasculino.isChecked()){
            PreferencesUtil.setString(getBaseContext(), "sexo", "Masculino");
        } else {
            PreferencesUtil.setString(getBaseContext(), "sexo", "Feminino");
        }
        finish();
    }
}
